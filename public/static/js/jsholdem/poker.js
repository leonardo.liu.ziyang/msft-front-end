/*
如果您改进了此软件或发现了一个错误，请让我知道：orciu@users.sourceforge.net
项目主页：http://sourceforge.net/projects/jsholdem/
*/
"use strict";

var START_DATE;
var NUM_ROUNDS;
var STOP_AUTOPLAY = 0;
var RUN_EM = 0;
var STARTING_BANKROLL = 500;
var SMALL_BLIND;
var BIG_BLIND;
var BG_HILITE = "gold"; // "#EFEF30",
var global_speed = 1;
var HUMAN_WINS_AGAIN;
var HUMAN_GOES_ALL_IN;
var cards = new Array(52);
var players;
var board, deck_index, button_index;
var current_bettor_index, current_bet_amount, current_min_raise;

function leave_pseudo_alert() {
  gui_write_modal_box("");
}

function my_pseudo_alert(text) {
  var html =
    "<html><body topmargin=2 bottommargin=0 bgcolor=" +
    BG_HILITE +
    " onload='document.f.y.focus();'>" +
    "<font size=+2>" +
    text +
    "</font><form name=f><input name=y type=button value='确定' " +
    "onclick='parent.leave_pseudo_alert()'></form></body></html>";
  gui_write_modal_box(html);
}

function player(name, bankroll, carda, cardb, status, total_bet, subtotal_bet) {
  this.name = name;
  this.bankroll = bankroll;
  this.carda = carda;
  this.cardb = cardb;
  this.status = status;
  this.total_bet = total_bet;
  this.subtotal_bet = subtotal_bet;
}

// 检查浏览器是否支持 localStorage
function has_local_storage() {
  try {
    var storage = window["localStorage"];
    var x = "__storage_test__";
    storage.setItem(x, x);
    storage.removeItem(x);
    return true;
  } catch (e) {
    return false;
  }
}

function init() {
  if (!has_local_storage()) {
    my_pseudo_alert(
      "您的浏览器不支持 localStorage - 请尝试使用更现代的浏览器，例如 Firefox"
    );
    return;
  }
  gui_hide_poker_table();
  gui_hide_log_window();
  gui_hide_setup_option_buttons();
  gui_hide_fold_call_click();
  gui_hide_guick_raise();
  gui_hide_dealer_button();
  gui_hide_game_response();
  gui_initialize_theme_mode();
  make_deck();
  new_game();
}

function make_deck() {
  var i;
  var j = 0;
  for (i = 2; i < 15; i++) {
    cards[j++] = "h" + i;
    cards[j++] = "d" + i;
    cards[j++] = "c" + i;
    cards[j++] = "s" + i;
  }
}

function handle_how_many_reply(opponents) {
  gui_write_modal_box("");
  write_settings_frame();
  new_game_continues(opponents);
  gui_initialize_css(); // 加载背景图像
  gui_show_game_response();
}

function ask_how_many_opponents() {
  var quick_values = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  var asking =
    "<b><font size=+12 color=red>" +
    "您想要几个人一起游戏？" +
    "</font></b><br>";
  for (var i = 0; i < 9; i++) {
    if (quick_values[i]) {
      asking +=
        "<font size=+12>" +
        "<a href='javascript:parent.handle_how_many_reply(" +
        quick_values[i] +
        ")'>" +
        quick_values[i] +
        " </a></font>" +
        "&nbsp;&nbsp;&nbsp;";
    }
  }
  var html9 = "<td><table align=center><tr><td align=center>";
  var html10 = asking + "</td></tr></table></td></tr></table></body></html>";
  gui_write_modal_box(html9 + html10);
}

function initialize_game() {
  gui_hide_poker_table();
  gui_hide_dealer_button();
  gui_hide_fold_call_click();
  gui_show_poker_table();
}

function clear_player_cards(count) {
  count = count + 1; // 包含玩家自己
  for (var pl = 0; pl < count; ++pl) {
    gui_set_player_cards("", "", pl);
    gui_set_player_name("", pl);
    gui_set_bet("", pl);
    gui_set_bankroll("", pl);
  }
}

function new_game() {
  START_DATE = new Date();
  NUM_ROUNDS = 0;
  HUMAN_WINS_AGAIN = 0;
  initialize_game();
  ask_how_many_opponents();
}

function new_game_continues(req_no_opponents) {
  var my_players = [
    new player("特朗普", 0, "", "", "", 0, 0),
    new player("埃隆·马斯克", 0, "", "", "", 0, 0),
    new player("莱昂纳多·达芬奇", 0, "", "", "", 0, 0),
    new player("昆同尼亚人", 0, "", "", "", 0, 0),
    new player("姜萍", 0, "", "", "", 0, 0),
    new player("王群", 0, "", "", "", 0, 0),
    new player("金正恩", 0, "", "", "", 0, 0),
    new player("黑神话·悟空", 0, "", "", "", 0, 0),
    new player("周润发", 0, "", "", "", 0, 0),
  ];

  players = new Array(req_no_opponents + 1);
  var player_name = getLocalStorage("playername");
  if (!player_name) {
    player_name = "玩家";
  }
  players[0] = new player(player_name, 0, "", "", "", 0, 0);
  my_players.sort(compRan);
  var i;
  for (i = 1; i < players.length; i++) {
    players[i] = my_players[i - 1];
  }
  clear_player_cards(my_players.length);
  reset_player_statuses(0);
  clear_bets();
  for (i = 0; i < players.length; i++) {
    players[i].bankroll = STARTING_BANKROLL;
  }
  button_index = Math.floor(Math.random() * players.length);
  new_round();
}

function number_of_active_players() {
  var num_playing = 0;
  var i;
  for (i = 0; i < players.length; i++) {
    if (has_money(i)) {
      num_playing += 1;
    }
  }
  return num_playing;
}

function new_round() {
  RUN_EM = 0;
  NUM_ROUNDS++;
  // 清除按钮
  gui_hide_fold_call_click();

  var num_playing = number_of_active_players();
  if (num_playing < 2) {
    gui_setup_fold_call_click("开始新游戏", 0, new_game, new_game);
    return;
  }
  HUMAN_GOES_ALL_IN = 0;
  reset_player_statuses(1);
  clear_bets();
  clear_pot();
  current_min_raise = 0;
  collect_cards();
  button_index = get_next_player_position(button_index, 1);
  var i;
  for (i = 0; i < players.length; i++) {
    write_player(i, 0, 0);
  }

  for (i = 0; i < board.length; i++) {
    if (i > 4) {
      // board.length != 5
      continue;
    }
    board[i] = "";
    gui_lay_board_card(i, board[i]); // 清空桌面
  }
  for (i = 0; i < 3; i++) {
    board[i] = "";
    gui_burn_board_card(i, board[i]);
  }

  var message = "<tr><td><font size=+2><b>新的一轮</b></font>";
  gui_write_game_response(message);
  gui_hide_guick_raise();
  shuffle();
  blinds_and_deal();
}

function collect_cards() {
  board = new Array(6);
  for (var i = 0; i < players.length; i++) {
    players[i].carda = "";
    players[i].cardb = "";
  }
}

function new_shuffle() {
  function get_random_int(max) {
    return Math.floor(Math.random() * max);
  }
  var len = cards.length;
  for (var i = 0; i < len; ++i) {
    var j = i + get_random_int(len - i);
    var tmp = cards[i];
    cards[i] = cards[j];
    cards[j] = tmp;
  }
}

function shuffle() {
  new_shuffle();
  deck_index = 0;
}

function blinds_and_deal() {
  SMALL_BLIND = 5;
  BIG_BLIND = 10;
  var num_playing = number_of_active_players();
  if (num_playing == 3) {
    SMALL_BLIND = 10;
    BIG_BLIND = 20;
  } else if (num_playing < 3) {
    SMALL_BLIND = 25;
    BIG_BLIND = 50;
  }
  var small_blind = get_next_player_position(button_index, 1);
  the_bet_function(small_blind, SMALL_BLIND);
  write_player(small_blind, 0, 0);
  var big_blind = get_next_player_position(small_blind, 1);
  the_bet_function(big_blind, BIG_BLIND);
  write_player(big_blind, 0, 0);
  players[big_blind].status = "OPTION";
  current_bettor_index = get_next_player_position(big_blind, 1);
  deal_and_write_a();
}

function unroll_player(starting_player, player_pos, final_call) {
  var next_player = get_next_player_position(player_pos, 1);
  write_player(player_pos, 0, 0);
  if (starting_player == next_player) {
    setTimeout(final_call, 550 * global_speed);
  } else {
    setTimeout(
      unroll_player,
      550 * global_speed,
      starting_player,
      next_player,
      final_call
    );
  }
}

function deal_and_write_a() {
  var current_player;
  var start_player;

  start_player = current_player = get_next_player_position(button_index, 1);
  // 给仍在游戏中的玩家发牌
  do {
    players[current_player].carda = cards[deck_index++];
    current_player = get_next_player_position(current_player, 1);
  } while (current_player != start_player);

  // 现在展示牌面
  current_player = get_next_player_position(button_index, 1);
  unroll_player(current_player, current_player, deal_and_write_b);
}

// 在开始下注前做一个小延迟
function delay_for_main() {
  setTimeout(main, 1000);
}

function deal_and_write_b() {
  var current_player = button_index;
  for (var i = 0; i < players.length; i++) {
    current_player = get_next_player_position(current_player, 1);
    if (players[current_player].cardb) {
      break;
    }
    players[current_player].cardb = cards[deck_index++];
  }

  current_player = get_next_player_position(button_index, 1);
  unroll_player(current_player, current_player, delay_for_main);
}

function go_to_betting() {
  if (get_num_betting() > 1) {
    setTimeout(main, 1000 * global_speed);
  } else {
    setTimeout(ready_for_next_card, 1000 * global_speed);
  }
}

function unroll_table(last_pos, current_pos, final_call) {
  gui_lay_board_card(current_pos, board[current_pos]);

  if (current_pos == last_pos) {
    setTimeout(final_call, 150 * global_speed);
  } else {
    setTimeout(
      unroll_table,
      150 * global_speed,
      last_pos,
      current_pos + 1,
      final_call
    );
  }
}

function deal_flop() {
  var burn = cards[deck_index++];
  burn = "blinded";
  gui_burn_board_card(0, burn);
  var message = "<tr><td><font size=+2><b>发翻牌</b></font>";
  gui_write_game_response(message);
  for (var i = 0; i < 3; i++) {
    board[i] = cards[deck_index++];
  }

  // 发前三张公共牌
  setTimeout(
    unroll_table,
    1000,
    /*last_pos*/ 2,
    /*start_pos*/ 0,
    go_to_betting
  );
}

function deal_fourth() {
  var burn = cards[deck_index++];
  burn = "blinded";
  gui_burn_board_card(1, burn);
  var message = "<tr><td><font size=+2><b>发转牌</b></font>";
  gui_write_game_response(message);
  board[3] = cards[deck_index++];

  // 发第四张公共牌
  setTimeout(
    unroll_table,
    1000,
    /*last_pos*/ 3,
    /*start_pos*/ 3,
    go_to_betting
  );
}

function deal_fifth() {
  var burn = cards[deck_index++];
  burn = "blinded";
  gui_burn_board_card(2, burn);
  var message = "<tr><td><font size=+2><b>发河牌</b></font>";
  gui_write_game_response(message);
  board[4] = cards[deck_index++];

  // 发第五张公共牌
  setTimeout(
    unroll_table,
    1000,
    /*last_pos*/ 4,
    /*start_pos*/ 4,
    go_to_betting
  );
}

function main() {
  gui_hide_guick_raise();
  var increment_bettor_index = 0;
  if (
    players[current_bettor_index].status == "BUST" ||
    players[current_bettor_index].status == "FOLD"
  ) {
    increment_bettor_index = 1;
  } else if (!has_money(current_bettor_index)) {
    players[current_bettor_index].status = "CALL";
    increment_bettor_index = 1;
  } else if (
    players[current_bettor_index].status == "CALL" &&
    players[current_bettor_index].subtotal_bet == current_bet_amount
  ) {
    increment_bettor_index = 1;
  } else {
    players[current_bettor_index].status = "";
    if (current_bettor_index == 0) {
      var call_button_text = "<u>跟</u>注";
      var fold_button_text = "<font color=red><u>弃</u>牌</font>";
      var to_call = current_bet_amount - players[0].subtotal_bet;
      if (to_call > players[0].bankroll) {
        to_call = players[0].bankroll;
      }
      call_button_text += " $" + to_call;
      var that_is_not_the_key_you_are_looking_for;
      if (to_call == 0) {
        call_button_text = "<u>过</u>牌";
        fold_button_text = 0;
        that_is_not_the_key_you_are_looking_for = function (key) {
          if (key == 67) {
            // 过牌
            human_call();
          } else {
            return true; // 不处理
          }
          return false;
        };
      } else {
        that_is_not_the_key_you_are_looking_for = function (key) {
          if (key == 67) {
            // 跟注
            human_call();
          } else if (key == 70) {
            // 弃牌
            human_fold();
          } else {
            return true; // 不处理
          }
          return false;
        };
      }
      // 设置快捷键
      var ret_function = function (key_event) {
        actual_function(key_event.keyCode, key_event);
      };

      var actual_function = function (key, key_event) {
        if (that_is_not_the_key_you_are_looking_for(key)) {
          return;
        }
        gui_disable_shortcut_keys(ret_function);
        if (key_event != null) {
          key_event.preventDefault();
        }
      };

      var do_fold = function () {
        actual_function(70, null);
      };
      var do_call = function () {
        actual_function(67, null);
      };
      // 激活快捷键
      gui_enable_shortcut_keys(ret_function);

      // 启用按钮
      gui_setup_fold_call_click(
        fold_button_text,
        call_button_text,
        do_fold,
        do_call
      );

      var quick_values = new Array(6);
      if (to_call < players[0].bankroll) {
        quick_values[0] = current_min_raise;
      }
      var quick_start = quick_values[0];
      if (quick_start < 20) {
        quick_start = 20;
      } else {
        quick_start = current_min_raise + 20;
      }
      var i;
      for (i = 0; i < 5; i++) {
        if (quick_start + 20 * i < players[0].bankroll) {
          quick_values[i + 1] = quick_start + 20 * i;
        }
      }
      var bet_or_raise = "下注";
      if (to_call > 0) {
        bet_or_raise = "加注";
      }
      var quick_bets = "<b>快速" + bet_or_raise + "</b><br>";
      for (i = 0; i < 6; i++) {
        if (quick_values[i]) {
          quick_bets +=
            "<a href='javascript:parent.handle_human_bet(" +
            quick_values[i] +
            ")'>" +
            quick_values[i] +
            "</a>" +
            "&nbsp;&nbsp;&nbsp;";
        }
      }
      quick_bets +=
        "<a href='javascript:parent.handle_human_bet(" +
        players[0].bankroll +
        ")'>全押！</a>";
      var html9 = "<td><table align=center><tr><td align=center>";
      var html10 =
        quick_bets + "</td></tr></table></td></tr></table></body></html>";
      gui_write_guick_raise(html9 + html10);

      var hi_lite_color = gui_get_theme_mode_highlite_color();
      var message =
        "<tr><td><font size=+2><b>当前下注: " +
        current_bet_amount +
        "</b><br> 你需要再下注 <font color=" +
        hi_lite_color +
        " size=+3>" +
        to_call +
        "</font> 来跟注。</font></td></tr>";
      gui_write_game_response(message);
      write_player(0, 1, 0);
      return;
    } else {
      write_player(current_bettor_index, 1, 0);
      setTimeout(bet_from_bot, 777 * global_speed, current_bettor_index);
      return;
    }
  }
  var can_break = true;
  for (var j = 0; j < players.length; j++) {
    var s = players[j].status;
    if (s == "OPTION") {
      can_break = false;
      break;
    }
    if (s != "BUST" && s != "FOLD") {
      if (has_money(j) && players[j].subtotal_bet < current_bet_amount) {
        can_break = false;
        break;
      }
    }
  }
  if (increment_bettor_index) {
    current_bettor_index = get_next_player_position(current_bettor_index, 1);
  }
  if (can_break) {
    setTimeout(ready_for_next_card, 999 * global_speed);
  } else {
    setTimeout(main, 999 * global_speed);
  }
}

var global_pot_remainder = 0;

function handle_end_of_round() {
  var candidates = new Array(players.length);
  var allocations = new Array(players.length);
  var winning_hands = new Array(players.length);
  var my_total_bets_per_player = new Array(players.length);

  // 清除弃牌或破产的玩家
  var i;
  var still_active_candidates = 0;
  for (i = 0; i < candidates.length; i++) {
    allocations[i] = 0;
    my_total_bets_per_player[i] = players[i].total_bet;
    if (players[i].status != "FOLD" && players[i].status != "BUST") {
      candidates[i] = players[i];
      still_active_candidates += 1;
    }
  }

  var my_total_pot_size = get_pot_size();
  var my_best_hand_name = "";
  var best_hand_players;
  var current_pot_to_split = 0;
  var pot_remainder = 0;
  if (global_pot_remainder) {
    gui_log_to_history("转移累积底池剩余金额 " + global_pot_remainder);
    pot_remainder = global_pot_remainder;
    my_total_pot_size += global_pot_remainder;
    global_pot_remainder = 0;
  }

  while (my_total_pot_size > pot_remainder + 0.9 && still_active_candidates) {
    // 首先，未弃牌或破产的玩家都是候选人
    var winners = get_winners(candidates);
    if (!best_hand_players) {
      best_hand_players = winners;
    }
    if (!winners) {
      my_pseudo_alert("没有赢家");
      pot_remainder = my_total_pot_size;
      my_total_pot_size = 0;
      break;
    }

    // 获取赢家中最小的下注金额，例如全押
    var lowest_winner_bet = my_total_pot_size * 2;
    var num_winners = 0;
    for (i = 0; i < winners.length; i++) {
      if (!winners[i]) {
        continue;
      }
      if (!my_best_hand_name) {
        my_best_hand_name = winners[i]["hand_name"];
      }
      num_winners++;
      if (my_total_bets_per_player[i] < lowest_winner_bet) {
        lowest_winner_bet = my_total_bets_per_player[i];
      }
    }

    // 组成底池
    current_pot_to_split = pot_remainder;
    pot_remainder = 0;

    for (i = 0; i < players.length; i++) {
      if (lowest_winner_bet >= my_total_bets_per_player[i]) {
        current_pot_to_split += my_total_bets_per_player[i];
        my_total_bets_per_player[i] = 0;
      } else {
        current_pot_to_split += lowest_winner_bet;
        my_total_bets_per_player[i] -= lowest_winner_bet;
      }
    }

    // 平分底池
    var share = Math.floor(current_pot_to_split / num_winners);
    pot_remainder = current_pot_to_split - share * num_winners;

    for (i = 0; i < winners.length; i++) {
      if (my_total_bets_per_player[i] < 0.01) {
        candidates[i] = null; // 你已经获得了你的份额
      }
      if (!winners[i]) {
        continue;
      }
      my_total_pot_size -= share; // 从底池中扣除
      allocations[i] += share; // 给赢家
      winning_hands[i] = winners[i].hand_name;
    }

    // 迭代直到底池为零或没有更多的候选人
    still_active_candidates = 0;
    for (i = 0; i < candidates.length; i++) {
      if (candidates[i] == null) {
        continue;
      }
      still_active_candidates += 1;
    }
    if (still_active_candidates == 0) {
      pot_remainder = my_total_pot_size;
    }
    gui_log_to_history("结束迭代");
  } // 底池分配结束

  global_pot_remainder = pot_remainder;
  pot_remainder = 0;
  var winner_text = "";
  var human_loses = 0;
  // 分配底池并处理结果
  for (i = 0; i < allocations.length; i++) {
    if (allocations[i] > 0) {
      var a_string = "" + allocations[i];
      var dot_index = a_string.indexOf(".");
      if (dot_index > 0) {
        a_string = "" + a_string + "00";
        allocations[i] = a_string.substring(0, dot_index + 3) - 0;
      }
      winner_text +=
        winning_hands[i] +
        " 给 " +
        players[i].name +
        " " +
        allocations[i] +
        "。 ";
      players[i].bankroll += allocations[i];
      if (best_hand_players[i]) {
        // function write_player(n, hilite, show_cards)
        write_player(i, 2, 1);
      } else {
        write_player(i, 1, 1);
      }
    } else {
      if (!has_money(i) && players[i].status != "BUST") {
        players[i].status = "BUST";
        if (i == 0) {
          human_loses = 1;
        }
      }
      if (players[i].status != "FOLD") {
        write_player(i, 0, 1);
      }
    }
  }
  // 判断玩家是否获胜
  if (allocations[0] > 5) {
    HUMAN_WINS_AGAIN++;
  } else {
    HUMAN_WINS_AGAIN = 0;
  }

  var detail = "";
  for (i = 0; i < players.length; i++) {
    if (players[i].total_bet == 0 && players[i].status == "BUST") {
      continue; // 跳过破产的玩家
    }
    detail +=
      players[i].name +
      " 下注了 " +
      players[i].total_bet +
      "，获得了 " +
      allocations[i] +
      "。\n";
  }
  detail = " (<a href='javascript:alert(\"" + detail + "\")'>详情</a>)";

  var quit_text = "<font color=red>重新开始</font>";
  var quit_func = new_game;
  var continue_text = "<font color=green>继续</font>";
  var continue_func = new_round;

  if (players[0].status == "BUST" && !human_loses) {
    continue_text = 0;
    quit_func = function () {
      parent.STOP_AUTOPLAY = 1;
    };
    setTimeout(autoplay_new_round, 1500 + 1100 * global_speed);
  }

  var num_playing = number_of_active_players();
  if (num_playing < 2) {
    // 找到仍在游戏的玩家并给予底池
    for (i = 0; i < players.length; i++) {
      if (has_money(i)) {
        players[i].bankroll += pot_remainder;
        pot_remainder = 0;
      }
    }
  }
  if (pot_remainder) {
    var local_text = "有 " + pot_remainder + " 放入下一个底池\n";
    detail += local_text;
  }
  var hi_lite_color = gui_get_theme_mode_highlite_color();
  var html =
    "<html><body topmargin=2 bottommargin=0 bgcolor=" +
    BG_HILITE +
    " onload='document.f.c.focus();'><table><tr><td>" +
    get_pot_size_html() +
    "</td></tr></table><br><font size=+2 color=" +
    hi_lite_color +
    "><b>胜利：" +
    winner_text +
    "</b></font>" +
    detail +
    "<br>";
  gui_write_game_response(html);

  gui_setup_fold_call_click(quit_text, continue_text, quit_func, continue_func);

  var elapsed_milliseconds = new Date() - START_DATE;
  var elapsed_time = makeTimeString(elapsed_milliseconds);

  if (human_loses == 1) {
    var ending = NUM_ROUNDS == 1 ? "1 局。" : NUM_ROUNDS + " 局。";
    my_pseudo_alert(
      "抱歉，你破产了，" +
        players[0].name +
        "。\n\n" +
        elapsed_time +
        "，" +
        ending
    );
  } else {
    num_playing = number_of_active_players();
    if (num_playing < 2) {
      var end_msg = "游戏结束！";
      var over_ending = NUM_ROUNDS == 1 ? "1 局。" : NUM_ROUNDS + " 局。";
      if (has_money(0)) {
        end_msg += "\n\n你赢了，" + players[0].name.toUpperCase() + "！！！";
      } else {
        end_msg += "\n\n抱歉，你输了。";
      }
      my_pseudo_alert(
        end_msg + "\n\n本局游戏持续了 " + elapsed_time + "，" + over_ending
      );
    }
  }
}

function autoplay_new_round() {
  if (STOP_AUTOPLAY > 0) {
    STOP_AUTOPLAY = 0;
    new_game();
  } else {
    new_round();
  }
}

function ready_for_next_card() {
  var num_betting = get_num_betting();
  var i;
  for (i = 0; i < players.length; i++) {
    players[i].total_bet += players[i].subtotal_bet;
  }
  clear_bets();
  if (board[4]) {
    handle_end_of_round();
    return;
  }
  current_min_raise = BIG_BLIND;
  reset_player_statuses(2);
  if (players[button_index].status == "FOLD") {
    players[get_next_player_position(button_index, -1)].status = "OPTION";
  } else {
    players[button_index].status = "OPTION";
  }
  current_bettor_index = get_next_player_position(button_index, 1);
  var show_cards = 0;
  if (num_betting < 2) {
    show_cards = 1;
  }

  if (!RUN_EM) {
    for (i = 0; i < players.length; i++) {
      // <-- 展示玩家的牌
      if (players[i].status != "BUST" && players[i].status != "FOLD") {
        write_player(i, 0, show_cards);
      }
    }
  }

  if (num_betting < 2) {
    RUN_EM = 1;
  }
  if (!board[0]) {
    deal_flop();
  } else if (!board[3]) {
    deal_fourth();
  } else if (!board[4]) {
    deal_fifth();
  }
}

function the_bet_function(player_index, bet_amount) {
  if (players[player_index].status == "FOLD") {
    return 0;
    // 弃牌
  } else if (bet_amount >= players[player_index].bankroll) {
    // 全押
    bet_amount = players[player_index].bankroll;

    var old_current_bet = current_bet_amount;

    if (players[player_index].subtotal_bet + bet_amount > current_bet_amount) {
      current_bet_amount = players[player_index].subtotal_bet + bet_amount;
    }

    // current_min_raise 应该更早计算？ <--
    var new_current_min_raise = current_bet_amount - old_current_bet;
    if (new_current_min_raise > current_min_raise) {
      current_min_raise = new_current_min_raise;
    }
    players[player_index].status = "CALL";
  } else if (
    bet_amount + players[player_index].subtotal_bet ==
    current_bet_amount
  ) {
    // 跟注
    players[player_index].status = "CALL";
  } else if (
    current_bet_amount >
    players[player_index].subtotal_bet + bet_amount
  ) {
    // 下注太少
    if (player_index == 0) {
      my_pseudo_alert(
        "当前需要匹配的下注是 " +
          current_bet_amount +
          "\n你必须至少下注 " +
          (current_bet_amount - players[player_index].subtotal_bet) +
          " 或者弃牌。"
      );
    }
    return 0;
  } else if (
    bet_amount + players[player_index].subtotal_bet > current_bet_amount && // 加注太少
    get_pot_size() > 0 &&
    bet_amount + players[player_index].subtotal_bet - current_bet_amount <
      current_min_raise
  ) {
    if (player_index == 0) {
      my_pseudo_alert("最小加注目前是 " + current_min_raise + "。");
    }
    return 0;
  } else {
    // 加注
    players[player_index].status = "CALL";

    var previous_current_bet = current_bet_amount;
    current_bet_amount = players[player_index].subtotal_bet + bet_amount;

    if (get_pot_size() > 0) {
      current_min_raise = current_bet_amount - previous_current_bet;
      if (current_min_raise < BIG_BLIND) {
        current_min_raise = BIG_BLIND;
      }
    }
  }
  players[player_index].subtotal_bet += bet_amount;
  players[player_index].bankroll -= bet_amount;
  var current_pot_size = get_pot_size();
  gui_write_basic_general(current_pot_size);
  return 1;
}

function human_call() {
  // 清除按钮
  gui_hide_fold_call_click();
  players[0].status = "CALL";
  current_bettor_index = get_next_player_position(0, 1);
  the_bet_function(0, current_bet_amount - players[0].subtotal_bet);
  write_player(0, 0, 0);
  main();
}

function handle_human_bet(bet_amount) {
  if (bet_amount < 0 || isNaN(bet_amount)) bet_amount = 0;
  var to_call = current_bet_amount - players[0].subtotal_bet;
  bet_amount += to_call;
  var is_ok_bet = the_bet_function(0, bet_amount);
  if (is_ok_bet) {
    players[0].status = "CALL";
    current_bettor_index = get_next_player_position(0, 1);
    write_player(0, 0, 0);
    main();
    gui_hide_guick_raise();
  } else {
    crash_me();
  }
}

function human_fold() {
  players[0].status = "FOLD";
  // 清除按钮
  gui_hide_fold_call_click();
  current_bettor_index = get_next_player_position(0, 1);
  write_player(0, 0, 0);
  var current_pot_size = get_pot_size();
  gui_write_basic_general(current_pot_size);
  main();
}

function bet_from_bot(x) {
  var b = 0;
  var n = current_bet_amount - players[x].subtotal_bet;
  if (!board[0]) b = bot_get_preflop_bet();
  else b = bot_get_postflop_bet();
  if (b >= players[x].bankroll) {
    // 全押
    players[x].status = "";
  } else if (b < n) {
    // 下注太少
    b = 0;
    players[x].status = "FOLD";
  } else if (b == n) {
    // 跟注
    players[x].status = "CALL";
  } else if (b > n) {
    if (b - n < current_min_raise) {
      // 加注太少
      b = n;
      players[x].status = "CALL";
    } else {
      players[x].status = ""; // 加注
    }
  }
  if (the_bet_function(x, b) == 0) {
    players[x].status = "FOLD";
    the_bet_function(x, 0);
  }
  write_player(current_bettor_index, 0, 0);
  current_bettor_index = get_next_player_position(current_bettor_index, 1);
  main();
}

function write_player(n, hilite, show_cards) {
  var carda = "";
  var cardb = "";
  var name_background_color = "";
  var name_font_color = "";
  if (hilite == 1) {
    // 当前玩家
    name_background_color = BG_HILITE;
    name_font_color = "black";
  } else if (hilite == 2) {
    // 赢家
    name_background_color = "red";
  }
  if (players[n].status == "FOLD") {
    name_font_color = "black";
    name_background_color = "gray";
  }
  if (players[n].status == "BUST") {
    name_font_color = "white";
    name_background_color = "black";
  }
  gui_hilite_player(name_background_color, name_font_color, n);

  var show_folded = false;
  // 如果玩家已出局
  if (players[0].status == "BUST" || players[0].status == "FOLD") {
    show_cards = 1;
  }
  if (players[n].carda) {
    if (players[n].status == "FOLD") {
      carda = "";
      show_folded = true;
    } else {
      carda = "blinded";
    }
    if (n == 0 || (show_cards && players[n].status != "FOLD")) {
      carda = players[n].carda;
    }
  }
  if (players[n].cardb) {
    if (players[n].status == "FOLD") {
      cardb = "";
      show_folded = true;
    } else {
      cardb = "blinded";
    }
    if (n == 0 || (show_cards && players[n].status != "FOLD")) {
      cardb = players[n].cardb;
    }
  }
  if (n == button_index) {
    gui_place_dealer_button(n);
  }
  var bet_text = "TO BE OVERWRITTEN";
  var allin = "下注：";

  if (players[n].status == "FOLD") {
    bet_text =
      "弃牌 (" + (players[n].subtotal_bet + players[n].total_bet) + ")";
    if (n == 0) {
      HUMAN_GOES_ALL_IN = 0;
    }
  } else if (players[n].status == "BUST") {
    bet_text = "破产";
    if (n == 0) {
      HUMAN_GOES_ALL_IN = 0;
    }
  } else if (!has_money(n)) {
    bet_text =
      "全押 (" + (players[n].subtotal_bet + players[n].total_bet) + ")";
    if (n == 0) {
      HUMAN_GOES_ALL_IN = 1;
    }
  } else {
    bet_text =
      allin +
      "$" +
      players[n].subtotal_bet +
      " (" +
      (players[n].subtotal_bet + players[n].total_bet) +
      ")";
  }

  gui_set_player_name(players[n].name, n); // 座位索引从 0 开始
  gui_set_bet(bet_text, n);
  gui_set_bankroll(players[n].bankroll, n);
  gui_set_player_cards(carda, cardb, n, show_folded);
}

function make_readable_rank(r) {
  if (r < 11) {
    return r;
  } else if (r == 11) {
    return "J";
  } else if (r == 12) {
    return "Q";
  } else if (r == 13) {
    return "K";
  } else if (r == 14) {
    return "A";
  }
}

function get_pot_size() {
  var p = 0;
  for (var i = 0; i < players.length; i++) {
    p += players[i].total_bet + players[i].subtotal_bet;
  }
  return p;
}

function get_pot_size_html() {
  return "<font size=+4><b>总底池：" + get_pot_size() + "</b></font>";
}

function clear_bets() {
  for (var i = 0; i < players.length; i++) {
    players[i].subtotal_bet = 0;
  }
  current_bet_amount = 0;
}

function clear_pot() {
  for (var i = 0; i < players.length; i++) {
    players[i].total_bet = 0;
  }
}

function reset_player_statuses(type) {
  for (var i = 0; i < players.length; i++) {
    if (type == 0) {
      players[i].status = "";
    } else if (type == 1 && players[i].status != "BUST") {
      players[i].status = "";
    } else if (
      type == 2 &&
      players[i].status != "FOLD" &&
      players[i].status != "BUST"
    ) {
      players[i].status = "";
    }
  }
}

function get_num_betting() {
  var n = 0;
  for (var i = 0; i < players.length; i++) {
    if (
      players[i].status != "FOLD" &&
      players[i].status != "BUST" &&
      has_money(i)
    ) {
      n++;
    }
  }
  return n;
}

function change_name() {
  var name = prompt("你的名字是？", getLocalStorage("playername"));
  if (!name) {
    return;
  }
  if (!players) {
    my_pseudo_alert("现在获取名字太早了");
    return;
  }
  if (name.length > 14) {
    my_pseudo_alert("名字太长了，我会叫你小明");
    name = "小明";
  }
  players[0].name = name;
  write_player(0, 0, 0);
  setLocalStorage("playername", name);
}

function help_func() {
  var win = window.open("help.html", "_blank");
  win.focus();
}

function update_func() {
  var url = "https://sourceforge.net/projects/js-css-poker/files/";
  var win = window.open(url, "_blank");
  win.focus();
}

function write_settings_frame() {
  var default_speed = 2;
  var speed_i = getLocalStorage("gamespeed");
  if (speed_i == "") {
    speed_i = default_speed;
  }
  if (
    speed_i == null ||
    (speed_i != 0 &&
      speed_i != 1 &&
      speed_i != 2 &&
      speed_i != 3 &&
      speed_i != 4)
  ) {
    speed_i = default_speed;
  }
  set_speed(speed_i);
  gui_setup_option_buttons(
    change_name,
    set_raw_speed,
    help_func,
    update_func,
    gui_toggle_the_theme_mode
  );
}

function index2speed(index) {
  var speeds = ["2", "1", ".6", ".3", "0.01"];
  return speeds[index];
}

function set_speed(index) {
  global_speed = index2speed(index);
  setLocalStorage("gamespeed", index);
  gui_set_selected_speed_option(index);
}

function set_raw_speed(selector_index) {
  // 检查 selector_index = [1,5]
  if (selector_index < 1 || selector_index > 5) {
    my_pseudo_alert("无法将速度设置为 " + selector_index);
    selector_index = 3;
  }
  var index = selector_index - 1;
  set_speed(index);
}

function get_next_player_position(i, delta) {
  var j = 0;
  var step = 1;
  if (delta < 0) step = -1;

  var loop_on = 0;
  do {
    i += step;
    if (i >= players.length) {
      i = 0;
    } else {
      if (i < 0) {
        i = players.length - 1;
      }
    }

    // 检查是否可以停止
    loop_on = 0;
    if (players[i].status == "BUST") loop_on = 1;
    if (players[i].status == "FOLD") loop_on = 1;
    if (++j < delta) loop_on = 1;
  } while (loop_on);

  return i;
}

function getLocalStorage(key) {
  return localStorage.getItem(key);
}

function setLocalStorage(key, value) {
  return localStorage.setItem(key, value);
}

function has_money(i) {
  if (players[i].bankroll >= 0.01) {
    return true;
  }
  return false;
}

function compRan() {
  return 0.5 - Math.random();
}

function my_local_subtime(invalue, fractionizer) {
  var quotient = 0;
  var remainder = invalue;
  if (invalue > fractionizer) {
    quotient = Math.floor(invalue / fractionizer);
    remainder = invalue - quotient * fractionizer;
  }
  return [quotient, remainder];
}

function getTimeText(string, number, text) {
  if (number == 0) return string;
  if (string.length > 0) {
    string += " ";
  }
  if (number == 1) {
    string = string + "1 " + text;
  } else {
    string = string + number + " " + text + "s";
  }
  return string;
}

function makeTimeString(milliseconds) {
  var _MS_PER_SECOND = 1000;
  var _MS_PER_MINUTE = 1000 * 60;
  var _MS_PER_HOUR = _MS_PER_MINUTE * 60;
  var _MS_PER_DAY = 1000 * 60 * 60 * 24;
  var _MS_PER_WEEK = _MS_PER_DAY * 7;
  var weeks = 0;
  var days = 0;
  var hours = 0;
  var minutes = 0;
  var seconds = 0;
  [weeks, milliseconds] = my_local_subtime(milliseconds, _MS_PER_WEEK);
  [days, milliseconds] = my_local_subtime(milliseconds, _MS_PER_DAY);
  [hours, milliseconds] = my_local_subtime(milliseconds, _MS_PER_HOUR);
  [minutes, milliseconds] = my_local_subtime(milliseconds, _MS_PER_MINUTE);
  [seconds, milliseconds] = my_local_subtime(milliseconds, _MS_PER_SECOND);

  var string = "";
  string = getTimeText(string, weeks, "week");
  string = getTimeText(string, days, "day");
  string = getTimeText(string, hours, "hour");
  string = getTimeText(string, minutes, "minute");
  string = getTimeText(string, seconds, "second");

  return string;
}
