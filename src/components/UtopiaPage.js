// UtopiaPage.js
import React from "react";
import styles from "../styles/UtopiaPage.module.scss";

function UtopiaPage() {
  return (
    <div className={styles.utopiaPage}>
      <h2>Utopia: Freedom & Democracy</h2>
      <img src="/utopia.png" alt="Utopia" className={styles.utopiaPage} />
      {
        <em>
          Not like the brazen giant of Greek fame, With conquering limbs astride
          from land to land; Here at our sea-washed, sunset gates shall stand A
          mighty woman with a torch, whose flame Is the imprisoned lightning,
          and her name Mother of Exiles. From her beacon-hand Glows world-wide
          welcome; her mild eyes command The air-bridged harbor that twin cities
          frame. "Keep, ancient lands, your storied pomp!" cries she With silent
          lips. "Give me your tired, your poor, Your huddled masses yearning to
          breathe free, The wretched refuse of your teeming shore. Send these,
          the homeless, tempest-tost to me, I lift my lamp beside the golden
          door!" - Lazarus, Emma. “The New Colossus.” 1883.
        </em>
        /* 
      <p>
        I think it's normal. In fact, before attending university, I didn't have
        any significant prejudices about my own environment. This is just my
        personal perspective.
      </p>
      <p>
        Later, I went to the UK for my bachelor's degree and encountered various
        prejudices against Chinese people. At first, I thought these were very
        extreme, especially from some people from Hong Kong and Taiwan. They
        strongly disapproved of the Chinese identity and even harbored hatred
        towards it (from my experience). During this time, I also came to
        understand and began to face an identity crisis. Many overseas Chinese I
        met and interacted with also had identity crises. They would not
        identify as Chinese, preferring to use English names wherever possible.
        I think this is normal, not just limited to Chinese people. It's often
        due to a language barrier and not being able to integrate into local
        society.
      </p>
      <p>
        After graduating and working back in China for a while, I slowly began
        to understand them. In my time working in China, I realized that maybe
        what they said was true, like the lack of freedom of speech in China,
        and that most Chinese people lack deep cultivation (I feel like I was
        gradually influenced). Sometimes I feel that Chinese-funded enterprises
        also lack humanitarian care, constantly requiring overtime work, and
        having to work until midnight. However, there are also some who
        voluntarily choose to work late to create an illusion of hard work.
      </p>
      <p>
        I then tried to apply for jobs in the UK, and luckily, I returned to
        work there. In the UK, I felt that people had a very relaxed work-life
        balance. I felt that the people around me had "souls." Even the ordinary
        bus drivers would always ask if I was happy today or engage in other
        small talk. When walking on the streets, strangers who I had never met
        before would greet me. It seemed like everyone had a rich spiritual
        world; however, in China, my colleagues, although they might not be
        earning less, rarely seemed genuinely happy. Everyone seemed caught in
        an endless cycle of anxiety and I didn't feel they were physically or
        mentally healthy, showing signs of various hidden diseases, such as high
        uric acid, fatty liver, skin spots, etc., all due to work stress (I
        discovered this during a Friday night drink with colleagues).
      </p>
      <p>
        Therefore, I think I still believe in hedonism. I usually choose the
        best conditions for renting a house, choose the best food without caring
        about the price, of course, within my means. In China, I chose to live
        in a 5000 RMB per month loft apartment, which I was very satisfied with.
        It had a great environment and ample space, but my colleagues, who lived
        in company-provided apartments costing 1000-2000 RMB per month, which
        were old and in poor condition, would tease me for living so
        "luxuriously." Their commute time was over 30 minutes, whereas my
        apartment was right below the office building, allowing me to sleep
        until 9 am and still make it to work on time. I think this gave me an
        advantage at work. Better living conditions gave me more energy, right?
        More motivation also brought more creativity and improved efficiency. My
        colleagues had to get up early to catch the company bus and worry about
        clocking in (it's worth mentioning that the clocking system has a signal
        range, and my apartment was within this range, so I could clock in with
        my phone at home, avoiding attendance anxiety). They even earned more
        than me but chose not to live like this, even considering my lifestyle
        as extravagant, preferring to save money at the expense of their
        well-being.
      </p>
      <p>
        Also, while working in China, many people liked to give advice,
        regardless of who they were. They always felt they had more experience
        because they had worked longer, which I would say is bureaucracy. For
        example, a colleague of the same level told me that one should only
        consider changing jobs after working there for more than three years. I
        think no one in this world can give you advice because they are not you.
        As for the topic of job-hopping, I discussed it with colleagues when I
        worked in the UK. They believed that if a job brought more gains, it was
        worth pursuing. In the Confucian view of a career, there might be a
        layer of loyalty involved. If you frequently change employers, you may
        be considered disloyal. However, my Jewish-American boss once told me,
        "If my employees can't go to other companies for higher positions, I'd
        be disappointed." I don't think a company's success depends on employee
        loyalty.
      </p>
      <p>
        It's worth mentioning the strong hierarchy and various contempt chains
        that are rampant. When I first joined my first company as an intern, an
        HR sister came to receive me on my first day. Everything was fine until
        we got to the office. The first thing I heard from a colleague was -
        "Are they from our company? Or outsourced?" I didn't understand at the
        time, so I didn't pay much attention. In the end, I realized the company
        liked to hire cheap labor, such as outsourcing. Some jobs that don't
        require much skill are outsourced to other companies' employees, who sit
        next to us in the office, but you can tell from their employee numbers
        whether they are outsourced. In general, the company was filled with
        contempt for outsourced labor. However, during my tenure, I always
        treated outsourced employees as part of the company, eating with them,
        attending meetings, playing games together. These outsourced workers did
        the same amount of work as the company's employees, sometimes even more.
        So what I saw more of was arrogance and prejudice from the company's
        employees because they liked to hire students from good schools, leading
        to academic snobbery. And these "graduate" employees from "good schools"
        always spoke in a juvenile tone, which I found embarrassing for someone
        nearly 30 years old. This is also due to the sacred aura given by
        Chinese society to the major exams after high school, as if this exam
        determines your entire life, and has evolved into capital for boasting
        in society.
      </p>
      <p>
        What I find incomprehensible is that after graduating, most Chinese
        students dream of becoming civil servants, possibly guided by policy
        trends. Becoming a government official to some extent sacrifices the
        potential and possibilities of their career, choosing to accept
        mediocrity, lying flat and safe for the rest of their lives.
      </p>
      <p>
        Speaking of age, I think it's worth mentioning the so-called 35-year-old
        age crisis in Chinese society. Although in my view this just reflects
        the unfairness of the corporate recruitment process, I think it's
        normal. From my time working in Chinese companies, I might have been the
        only undergraduate, with no undergraduates on the entire floor (formal
        employees). Most of them join the company at 26-27 years old after
        graduate school, work for 3-5 years, and might not reach the management
        level, unless they are exceptionally outstanding, which is a small
        probability event. So when you join the company after graduate school
        and work for 5 years, you might just be in your early 30s, but you've
        only worked for five years. It's normal not to reach the management
        level, so you're out of the game by the time you're 35. If you're senior
        or in management by 35, you're irreplaceable, but if you're still at the
        junior level, it's normal to face this crisis.
      </p>
      <p>
        I feel that my values are constantly being challenged. In the Chinese
        environment, I feel contradictions, as moral views and various concepts
        are inconsistent. This makes me feel "awake" while those around me seem
        "asleep." I think I see the negative side of society, and due to the
        government and various social media "propaganda," everyone feels like
        they're being "brainwashed." Although I think this is the same in
        Western society, I still feel that Chinese people live a "tired" life,
        and voluntarily so, losing many other more meaningful things in the
        process. There are no unions here, and workers' rights are almost zero,
        with no one speaking up for them. However, in the UK, a walk around can
        make you feel the local humanistic atmosphere, undoubtedly happy and
        content. As I mentioned before, a bus driver who might not earn much,
        but I feel he lives happily, and because he's happy, I also feel happy.
        On the street, you'll receive prayers from believers and greetings from
        strangers. However, in China, crossing a street is a challenge, with no
        vehicles willing to give way. What's the rush? And pedestrians, if you
        walk fast and someone in front blocks your way, they'll pretend not to
        see you. I feel I'm developing road rage; the service industry has
        almost no service spirit, no consideration for others, and waiters will
        act petulantly, like children. If you don't speak while ordering, they
        won't initiate conversation. It's worth mentioning that everyone in
        China has the attitude of a boss, loving to compare everywhere. I think
        Chinese people will never learn the spirit of serving others, always
        wanting to be the ones served. So I think the average spiritual level of
        this country is not affluent. I feel that as a person, they lack some
        qualities, very different from the Confucian teachings of being a
        country of etiquette.
      </p>
      <p>
        Regrettably, my time working in the UK was not long. In June, as I was
        about to leave the UK, I stayed with an Italian family in London. It's
        worth noting that Italians have a strong sense of family. The host would
        call his mother every evening. It was a large, affectionate family with
        many relatives living together, creating a joyous and harmonious
        atmosphere in the home. They even invited me to lunch to try their rolls
        (a type of vegetarian wrap), and I was given a room of my own. They had
        many toddlers who were just learning to walk, but they weren't
        disruptive and had excellent manners as hosts. They always knocked
        before entering, showing great respect for personal space. However, upon
        returning to China, the behavior of children downstairs in the building
        seemed quite different, often acting like "little emperors." They would
        stand still when seeing you walking towards them, as if expecting you to
        walk around them so they could continue in a straight line. I suspect
        this "little emperor" attitude is nurtured from a young age, making it
        hard to change in society. This could be how the spirit of service is
        being eroded.
      </p>
      <p>
        It's regrettable that Chinese people are constantly comparing themselves
        with others, as if the source of happiness lies in being better off than
        someone else. If I am doing better than you, I feel happy. More
        absurdly, seeing others doing better than oneself often leads to
        jealousy. Living in a world of constant comparison, rather than simply
        living in the world. In short, no one wishes you to be better off than
        them; they hope you are worse off. This endless comparison, once
        started, means you have already lost.
      </p>
      <p>
        I consider myself fortunate to have retained a certain adaptability,
        perhaps due to my profession. I'm always learning, constantly
        challenging myself, absorbing new technologies, and adapting to new
        environments. I remain optimistic and open to different viewpoints,
        never "fixating" on certain principles, preferring to critically examine
        things rather than following the crowd. Having judgment is important
        because that is what makes a "person."
      </p>
      <p>
        In conclusion, a person is ultimately a product of their environment.
        The environment shapes the kind of person one becomes. This could be
        seen as a conflict of ideologies, but I prefer to believe that I am
        "awake."
      </p> */
      }
    </div>
  );
}

export default UtopiaPage;
