import React from "react";
import "../styles/Footer.scss";

function Footer() {
  return (
    <footer className="blog-footer">
      <p>Copyright &copy; {new Date().getFullYear()}</p>
    </footer>
  );
}

export default Footer;
