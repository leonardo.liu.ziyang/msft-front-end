import React, { useState, useEffect } from "react";
import styles from "../styles/CommercialPlans.module.scss"; // Adjust this path if necessary
import ReactMarkdown from "react-markdown";

function CommercialPlans() {
  const [markdown, setMarkdown] = useState("");

  useEffect(() => {
    fetch("../.md")
      .then((res) => res.text())
      .then((text) => setMarkdown(text))
      .catch((err) => console.error(err));
  }, []);

  return (
    <div className={styles.commercialPlans}>
      <ReactMarkdown>{markdown}</ReactMarkdown>
    </div>
  );
}

export default CommercialPlans;
