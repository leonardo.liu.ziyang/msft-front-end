import React, { useState, useEffect } from "react";
import styles from "../styles/LucidDream.scss";
import ReactMarkdown from "react-markdown";

function LucidDream() {
  const [markdown, setMarkdown] = useState("");

  useEffect(() => {
    fetch("../lucid-dream.md")
      .then((res) => res.text())
      .then((text) => setMarkdown(text))
      .catch((err) => console.error(err));
  }, []);

  return (
    <div className={styles.lucidDream}>
      <ReactMarkdown>{markdown}</ReactMarkdown>
    </div>
  );
}

export default LucidDream;
