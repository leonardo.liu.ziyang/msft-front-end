import React, { useState } from "react";
import { loadStripe } from "@stripe/stripe-js";
import {
  Elements,
  CardElement,
  useStripe,
  useElements,
} from "@stripe/react-stripe-js";
import "../styles/PaymentPage.scss"; // Add CSS for enhanced styles

const stripePromise = loadStripe(
  "pk_live_51QQuxOHTYLgy4RaZHYDHwFvs6K8tehbPXjEMchiqEqaYoSgcLDdTB3qA17Sqf9pE0ZiG1Rp4kasRZ2ZxukzRYq3H00iRxUiw3F"
);

const PaymentForm = ({ onPaymentSuccess }) => {
  const stripe = useStripe();
  const elements = useElements();
  const [paymentStatus, setPaymentStatus] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const handleSubmit = async (event) => {
    event.preventDefault();
    if (!stripe || !elements) return;

    setIsLoading(true);
    const cardElement = elements.getElement(CardElement);

    try {
      const response = await fetch(
        "/.netlify/functions/create-payment-intent",
        {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({ amount: 5000 }),
        }
      );

      const { clientSecret } = await response.json();
      const { paymentIntent, error } = await stripe.confirmCardPayment(
        clientSecret,
        {
          payment_method: {
            card: cardElement,
            billing_details: { name: "Customer Name" },
          },
        }
      );

      if (error) {
        setPaymentStatus(`Payment failed: ${error.message}`);
      } else if (paymentIntent.status === "succeeded") {
        setPaymentStatus("Payment succeeded!");
        localStorage.setItem("paymentCompleted", "true"); // 注意值是字符串 "true"
        onPaymentSuccess();
      }
    } catch (error) {
      setPaymentStatus(`Payment failed: ${error.message}`);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div className="payment-form">
      <h1>Complete Your Payment</h1>
      <form onSubmit={handleSubmit}>
        <CardElement />
        <button type="submit" disabled={!stripe || isLoading}>
          {isLoading ? "Processing..." : "Pay $50.00"}
        </button>
      </form>
      {paymentStatus && <p className="payment-status">{paymentStatus}</p>}
    </div>
  );
};

const PaymentPage = ({ onPaymentSuccess }) => (
  <Elements stripe={stripePromise}>
    <PaymentForm onPaymentSuccess={onPaymentSuccess} />
  </Elements>
);

export default PaymentPage;
