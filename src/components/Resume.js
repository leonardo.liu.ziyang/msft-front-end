import React, { useState, useEffect } from "react";
import { CSSTransition } from "react-transition-group";
import "../styles/Resume.scss";
import { getDatabase, ref, onValue, push } from "firebase/database";
import ReactMarkdown from "react-markdown";
import app from "./FirebaseConfig"; // 确保FirebaseConfig.js文件与Resume.js在同一目录下

function Resume() {
  const [activeTab, setActiveTab] = useState("home");
  const [inProp, setInProp] = useState(false);
  const [messages, setMessages] = useState([]);
  const [newMessage, setNewMessage] = useState("");
  const [isAuthorized, setIsAuthorized] = useState(false);
  const [showAuthPrompt, setShowAuthPrompt] = useState(false);
  const [markdownContent, setMarkdownContent] = useState(""); // State to hold markdown content

  const database = getDatabase(app);

  useEffect(() => {
    fetch("/idea.md")
      .then((response) => response.text())
      .then((text) => setMarkdownContent(text))
      .catch((error) => {
        console.error("Error fetching markdown:", error);
        setMarkdownContent("Failed to load content.");
      });
  }, []);

  const handleNewMessageChange = (e) => {
    setNewMessage(e.target.value);
  };

  const handleSubmitMessage = (e) => {
    e.preventDefault();
    if (newMessage.trim() !== "") {
      const messagesRef = ref(database, "messages");
      push(messagesRef, {
        text: newMessage,
        timestamp: Date.now(),
      });
      setNewMessage(""); // 清空输入框
    }
  };

  useEffect(() => {
    const messagesRef = ref(database, "messages");
    onValue(messagesRef, (snapshot) => {
      const data = snapshot.val();
      const loadedMessages = data
        ? Object.keys(data).map((key) => ({
            id: key,
            ...data[key],
          }))
        : [];
      setMessages(loadedMessages);
    });
  }, [database]);

  const content = {
    home: (
      <>
        <p>
          I'm Ziyang Liu, a self-taught software engineer. I graduated in 2021
          from the University of Bristol with a degree in Electrical and
          Electronic Engineering and soon after launched my career in software
          development. Since then, I’ve had the opportunity to work around the
          world, building impactful projects that touch people’s daily lives and
          of which I’m truly proud.
        </p>
        <p>
          I plan to use this platform to share my thoughts on various topics,
          including personal experiences, technology trends, and interesting
          insights (some trick shots) from my piece of work. Overall, this blog
          is a window into my view of the world.
        </p>
        <p>
          You may contact me through my email address at{" "}
          <a href="mailto:leonardo.liu.ziyang@gmail.com">
            leonardo.liu.ziyang@gmail.com
          </a>
          . Alternatively, you may reach me out from my{" "}
          <a href="https://www.linkedin.com/in/ziyangliu1/">Linkedin</a>.
        </p>

        <em>
          All opinions and content posted here are solely my own and do not
          represent the views or positions of my employer or any other
          affiliated entities.
        </em>
      </>
    ),
    posts: isAuthorized ? (
      <ReactMarkdown>{markdownContent}</ReactMarkdown>
    ) : (
      <p>You need to authorize to see the content.</p>
    ),
    about: (
      <>
        <h3>Ziyang Liu</h3>
        <p>
          Algorithm Enthusiast, Competitive Programmer, System Architect &
          Software Engineer.
        </p>
        <h4>Education</h4>
        <ul>
          <li>
            Oct. 2018 – Oct. 2021, University of Bristol, Bachelor of
            Engineering (Honors), Electrical & Electronic Engineering, First
            Class Honours.
          </li>
          <li>
            Oct. 2015 – Oct. 2017, Guangdong Country Garden School, Cambridge
            Examination A-levels (AAAB), 184 UCAS tariff points.
          </li>
        </ul>
        <h4>Work Experience</h4>
        <ul>
          <li>
            Senior Software Engineer @Alibaba Inc. (March. 2024 - Oct. 2023)
            <ul>
              <li>
                Developed a system that improved link discovery and coverage for
                major platforms, achieving a link coverage rate of over 90%.
              </li>
              <li>
                Spearheaded improvements in web search indexing, focusing on
                increasing index generation and validation accuracy.
              </li>
            </ul>
          </li>
          <li>
            Software Development Engineer @Amazon Prime Video (Oct. 2022 - Oct.
            2023)
            <ul>
              <li>
                Developed a custom testing strategy that reduced testing time by
                up to 44%.
              </li>
              <li>
                Participated in front-end UI development for internal pipelines.
              </li>
            </ul>
          </li>
          <li>
            Software Engineer @Huawei Technologies (Jun. 2021 – Oct. 2022)
            <ul>
              <li>
                Developed an enterprise search engine and online training course
                platform.
              </li>
              <li>
                Implemented structural code search and code recommendation
                systems.
              </li>
            </ul>
          </li>
        </ul>
        <h4>Contacts</h4>
        <p>
          Email:{" "}
          <a href="mailto:competitiveprogrammer@outlook.com">
            competitiveprogrammer@outlook.com
          </a>
        </p>
        <p>
          LinkedIn:{" "}
          <a href="https://www.linkedin.com/in/ziyangliu1/">
            linkedin.com/in/ziyangliu1
          </a>
        </p>
      </>
    ),
    chitchat: (
      <p>
        Leave me a message if you want to. (still working on this feature, stay
        tuned!)
      </p>
    ),
  };

  content.chitchat = (
    <>
      <p>Leave me a message if you want to :) </p>
      <div className="chitchat-form">
        <form onSubmit={handleSubmitMessage} className="chitchat-form">
          <textarea
            value={newMessage}
            onChange={handleNewMessageChange}
            placeholder="Type your message here..."
          />
          <button type="submit">Submit</button>
        </form>
      </div>
      <ul className="messages-list">
        {messages.map((message, index) => (
          <li key={message.id}>{message.text}</li> // 现在使用message对象的text属性，并用id作为key
        ))}
      </ul>
    </>
  );

  const handleClick = (tabName) => {
    if (tabName === "posts" && !isAuthorized) {
      setShowAuthPrompt(true);
      setActiveTab("home"); // Redirect to "home" if not authorized
    } else {
      setInProp(false);
      setTimeout(() => {
        setActiveTab(tabName);
        setInProp(true);
      }, 500);
    }
  };

  const handleFocus = (e) => {
    e.target.scrollIntoView({ behavior: "smooth", block: "center" });
  };

  const handleAuthorization = (input) => {
    const correctPassword = "汤颖儿"; // Replace with your password or logic for puzzle
    if (input === correctPassword) {
      setIsAuthorized(true);
      setShowAuthPrompt(false);
      handleClick("posts"); // Switch to "posts" after authorization
    } else {
      alert("Incorrect password, please try again."); // Alert for incorrect password
    }
  };

  return (
    <div className="resume-container">
      <div className="headings-container">
        <h2 className="resume-heading" onClick={() => handleClick("home")}>
          Home
        </h2>
        <h2 className="resume-heading" onClick={() => handleClick("posts")}>
          🔒Commercials🔒
        </h2>
        <h2 className="resume-heading" onClick={() => handleClick("about")}>
          Hire Me
        </h2>
        <h2 className="resume-heading" onClick={() => handleClick("chitchat")}>
          Chit-Chat
        </h2>
        <h2
          className="resume-heading"
          onClick={() => (window.location.href = "/poker.html")}
        >
          🃏Texas Hold'em🃏
        </h2>
      </div>

      {showAuthPrompt && (
        <div className="modal-overlay" onClick={() => setShowAuthPrompt(false)}>
          <div className="auth-prompt" onClick={(e) => e.stopPropagation()}>
            <h3>🔒 TOP TRADE SECRET 🔒</h3>
            <h3>Please Confirm Your Name Is on the Authorized List:</h3>
            <input
              type="text"
              onFocus={handleFocus}
              onKeyDown={(e) => {
                if (e.key === "Enter") {
                  handleAuthorization(e.target.value);
                }
              }}
            />
            <button
              onClick={() =>
                handleAuthorization(
                  document.querySelector(".auth-prompt input").value
                )
              }
            >
              Submit
            </button>
          </div>
        </div>
      )}

      <CSSTransition in={inProp} timeout={500} classNames="fade">
        <div className="resume-content">{content[activeTab]}</div>
      </CSSTransition>
    </div>
  );
}
export default Resume;
