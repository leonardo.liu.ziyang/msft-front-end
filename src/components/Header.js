import React from "react";
import "../styles/Header.scss";
import { Link } from "react-router-dom";

function Header() {
  return (
    <header className="blog-header">
      <Link to="/">
        <h1>Quintonia</h1>
      </Link>
    </header>
  );
}

export default Header;
