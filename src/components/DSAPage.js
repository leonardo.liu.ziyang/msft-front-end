import React, { useState, useEffect } from "react";
import styles from "../styles/Mind.module.scss";
import ReactMarkdown from 'react-markdown';

function DSAPage() {
  const [markdown, setMarkdown] = useState('');

  useEffect(() => {
    fetch('../AdvancedDSA.md')
      .then(res => res.text())
      .then(text => setMarkdown(text))
      .catch(err => console.error(err));
  }, []);

  return (
    <div className={styles.mindPage}>
      <ReactMarkdown>{markdown}</ReactMarkdown>
    </div>
  );
}

export default DSAPage;