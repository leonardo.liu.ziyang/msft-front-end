import React from "react";
import { Link } from "react-router-dom";
import "../styles/Home.scss";

function Home() {
  const posts = [
    {
      id: 1,
      title: "Utopia: Freedom & Democracy",
      content:
        "Criticism on ideologies, governments, laws, politics, systems & philosophies.",
    },
    {
      id: 2,
      title: "Enchanted Loom: The Mind",
      content: "Conscious Networks in Brains.",
    },
    {
      id: 3,
      title: "Advanced Data Structure & Algorithms",
      content: "Beyond the scope of fundamentals.",
    },
    {
      id: 4,
      title: "Interesting Tricks: Puzzles",
      content: "Estimation of Pi.",
    },
    {
      id: 5,
      title: "Project: Recommendation Algorithms & Systems",
      content:
        "Knowledge management & Support employees' growth and professional development, recommendations & data-driven.",
    },
    {
      id: 6,
      title: "Project: Tool Built for Accelerating Software Build Process",
      content:
        "Test Only What've Changed for any packages in order to save build time.",
    },
    {
      id: 7,
      title: "Project: Link Discovery for Modern Search Engine",
      content:
        "Discovering web-links from hypertext - mining child-parent relationship between links.",
    },
    {
      id: 8,
      title: "Commercial Plan: 1",
      content: "Trade Secret.",
    },
    {
      id: 9,
      title: "Lucid Dream: Control Dreaming Exercise",
      content:
        "People actually may train themselves to control what they want to dream about.",
    },
  ];

  return (
    <div className="blog-home">
      <h1 className="blog-title">Other Threads</h1> {/* Added Title */}
      <div className="blog-posts">
        {posts.map((post) => (
          <div key={post.id} className="blog-post">
            <h3>
              <Link to={`/${post.id}`}>{post.title}</Link>
            </h3>
            <em>{post.content}</em>
          </div>
        ))}
      </div>
    </div>
  );
}

export default Home;
