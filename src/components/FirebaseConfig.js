import { initializeApp } from "firebase/app";
import 'firebase/database';

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBWYm2KG9WhpAhAbMo6JVoVB_pFXbdHtLQ",
  authDomain: "front-end-4b45c.firebaseapp.com",
  databaseURL: "https://front-end-4b45c-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "front-end-4b45c",
  storageBucket: "front-end-4b45c.appspot.com",
  messagingSenderId: "476655956720",
  appId: "1:476655956720:web:86dad7186e84fad0a8c835",
  // measurementId: "G-DF4MTZY85K" // 如果不使用Analytics，这一行可以省略
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export default app;
