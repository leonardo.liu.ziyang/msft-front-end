import React, { useState, useEffect } from 'react';
import ReactMarkdown from 'react-markdown';
import remarkMath from 'remark-math';
import rehypeKatex from 'rehype-katex';
import 'katex/dist/katex.min.css'; // 引入 KaTeX CSS
import styles from '../styles/Mind.module.scss';

function InterestingQuestions() {
  const [markdown, setMarkdown] = useState('');

  useEffect(() => {
    fetch('../Interesting-Questions.md')
      .then((res) => res.text())
      .then((text) => setMarkdown(text))
      .catch((err) => console.error(err));
  }, []);

  return (
    <div className={styles.mindPage}>
      <ReactMarkdown
        children={markdown}
        remarkPlugins={[remarkMath]}
        rehypePlugins={[rehypeKatex]}
      />
    </div>
  );
}

export default InterestingQuestions;
