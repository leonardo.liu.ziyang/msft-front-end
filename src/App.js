import React from "react";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
  useLocation,
} from "react-router-dom";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Home from "./components/Home";
import Resume from "./components/Resume";
import UtopiaPage from "./components/UtopiaPage";
import MindPage from "./components/MindPage";
import "./App.scss";
import DSAPage from "./components/DSAPage";
import InterestingQuestions from "./components/Interesting-Questions";
import CommercialPlans from "./components/CommercialPlans";
import LucidDream from "./components/LucidDream";
import PaymentPage from "./components/PaymentPage";

function AppWrapper() {
  return (
    <Router>
      <App />
    </Router>
  );
}

function PaymentGuard({ children }) {
  const [isPaymentCompleted, setIsPaymentCompleted] = React.useState(false);

  React.useEffect(() => {
    // 检查支付状态
    const checkPaymentStatus = () => {
      const paymentStatus = localStorage.getItem("paymentCompleted") === "true";
      setIsPaymentCompleted(paymentStatus);
    };

    checkPaymentStatus(); // 初始化时检查一次
    window.addEventListener("storage", checkPaymentStatus); // 监听 localStorage 变化

    return () => {
      window.removeEventListener("storage", checkPaymentStatus); // 清理事件监听
    };
  }, []);

  if (!isPaymentCompleted) {
    // 如果支付未完成，重定向到支付页面
    return <Navigate to="/payment" replace />;
  }

  // 如果支付完成，渲染子组件
  return children;
}

function App() {
  const location = useLocation();
  return (
    <div className="App">
      <Header />
      {location.pathname === "/" && <Resume />}
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/1" element={<UtopiaPage />} />
        <Route path="/2" element={<MindPage />} />
        <Route path="/3" element={<DSAPage />} />
        <Route path="/4" element={<InterestingQuestions />} />
        <Route path="/8" element={<CommercialPlans />} />
        <Route path="/5" element={<LucidDream />} />
        <Route
          path="/payment"
          element={
            <PaymentPage
              onPaymentSuccess={() => (window.location.href = "/poker.html")}
            />
          }
        />
        <Route
          path="/poker.html"
          element={
            <PaymentGuard>
              <iframe
                src="/poker.html"
                title="Poker Game"
                style={{
                  width: "100%",
                  height: "100vh",
                  border: "none",
                }}
              />
            </PaymentGuard>
          }
        />
      </Routes>
      <Footer />
    </div>
  );
}

export default AppWrapper;
