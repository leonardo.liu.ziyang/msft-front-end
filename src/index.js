import React from "react";
import ReactDOM from "react-dom/client";
// import AppWrapper from "./App";

// 创建 React 18 的根节点
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<React.StrictMode>{/* <AppWrapper /> */}</React.StrictMode>);
