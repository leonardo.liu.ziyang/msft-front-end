# my Blog

public/
├── index.html
├── poker.html
├── static/
├────── css/
│ ├──── poker.css
│ ├──── normalize.css
├────── js/
│ ├──── jsholdem/
│ │ ├── poker.js
│ │ ├── hands.js
│ │ ├── bot.js
│ └──── gui_if.js
src/
├── index.js
├── App.js
├── App.scss
├── components/
├── styles/
