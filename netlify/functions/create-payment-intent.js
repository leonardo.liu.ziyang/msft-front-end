const stripe = require("stripe")(
  "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"
); // 替换为你的 Stripe Secret Key

exports.handler = async function (event, context) {
  try {
    const { amount } = JSON.parse(event.body); // 从请求中获取支付金额

    // 创建支付意图
    const paymentIntent = await stripe.paymentIntents.create({
      amount, // 金额，单位为最小货币单位（如美分）
      currency: "usd", // 可替换为你的货币类型
    });

    return {
      statusCode: 200,
      body: JSON.stringify({ clientSecret: paymentIntent.client_secret }),
    };
  } catch (error) {
    return {
      statusCode: 500,
      body: JSON.stringify({ error: error.message }),
    };
  }
};
