![Alt text](/pi.png "Mind")
# Estimation of PI

## Monte Carlo Simulation

Monte Carlo Simulation 是一种使用随机数来解决计算问题的方法。在估计π（圆周率）的情况下，你可以使用以下步骤来进行 Monte Carlo 模拟：

1. **理解基本概念**：想象一个边长为 1 的正方形，内部有一个半径为 0.5（即正方形边长的一半）的四分之一圆。由于圆的面积是 $\pi r^2$，这个四分之一圆的面积就是 $\frac{\pi}{4}$。正方形的面积是 1。如果你随机地在正方形内生成点，那些落在四分之一圆内的点占总点数的比例应该接近 $\frac{\pi}{4}$。

2. **生成随机点**：在程序中，生成大量随机点，每个点有两个坐标 (x, y)，这些坐标的值都在 0 和 1 之间。

3. **计算点是否在圆内**：对于每个点，如果它到原点 (0,0) 的距离小于或等于 0.5（即圆的半径），那么这个点就在圆内。可以通过计算 $x^2 + y^2$ 来判断这一点，如果 $x^2 + y^2 \leq 0.5^2$，则点在圆内。

4. **估计 π 的值**：统计落在圆内的点的数目，然后将其与总点数的比例乘以 4，即 $\pi \approx 4 \times \frac{\text{圆内点的数目}}{\text{总点数}}$。

5. **提高准确度**：增加随机点的数量可以提高估计的准确度。但是，需要注意的是，随着点数的增加，计算的时间也会增加。

这种方法的优点在于它简单直观，且容易实现。但是，它的准确度取决于生成点的数量，且随机性导致每次运行结果可能会有所不同。在软件工程面试中，这种方法展示了你对基本统计概念和编程技能的理解。

## Numerical Integration (Leibniz Formula)

The Leibniz formula for $\pi$ is an infinite series:

$$
\pi = 4 \sum_{k=0}^{\infty} \frac{(-1)^k}{2k+1}
$$

This series converges very slowly, but it can be used to estimate $\pi$ by summing a large number of terms.

Here's a basic algorithm for this method:
- Sum the series up to a large number of terms $n$.
- Alternate adding and subtracting fractions of the form $\frac{1}{2k+1}$.
- Multiply the final sum by 4 to estimate $\pi$.
