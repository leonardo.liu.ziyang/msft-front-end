# Data Structure & Algorithm (Advanced)

<!-- GFM-TOC -->

- [DSA](#General-Questions)

  - [Segment Tree](#1)
  - [Fenwick Tree (Binary Indexed Tree)](#2)
  - [QuadTree](#13)
  - [XOR-Trie](#3)

- [Graph]

  - [Transitive Closure - Floyd-Warshall algorithm](#4)
  - [Topological Sorting](#5)
  - [Prim-Jarnik's Algorithm (Greedy)](#5)
  - [Kruskal's Algorithm (Greedy)](#5)
  - [Dijkstra's Algorithm](#5)
  - [Union-Find](#2)
  - [Tarjan's Algorithm](#5)
  - [Bellman-Ford's Algorithm](#5)
  - [A-star Algorithm](#5)

- [Others]

  - [Sweep Line Algorithm](#7)

<span id = "1"></span>

## Segment Tree

```Python
class SegmentTree:
    def __init__(self, arr):
        self.n = len(arr)  # 获取输入数组的长度
        self.tree = [0] * (2 * self.n)  # 创建一个长度为2n的数组来存储段落树的所有节点

        # 初始化叶节点
        for i in range(self.n):
            self.tree[i + self.n] = arr[i]  # 将输入数组的元素直接放在树的后半部分，即树的叶节点

        # 从叶节点向上构建树的其他部分
        for i in range(self.n - 1, 0, -1):
            # 计算当前节点的值作为其子节点值的和
            self.tree[i] = self.tree[i << 1] + self.tree[i << 1 | 1]

    def update(self, pos, value):
        """ 更新位置pos的值为value """
        pos += self.n  # 转换原数组索引至树中的相应位置
        self.tree[pos] = value  # 更新叶节点
        while pos > 1:
            pos >>= 1  # 向上移动至父节点
            # 更新父节点的值为其两个子节点的值之和
            self.tree[pos] = self.tree[pos << 1] + self.tree[pos << 1 | 1]

    def query(self, l, r):
        """ 查询区间[l, r)的和 """
        l += self.n  # 将左端点转换为树中的索引
        r += self.n  # 将右端点转换为树中的索引
        sum = 0
        while l < r:
            if l & 1:
                sum += self.tree[l]  # 如果l是奇数，加上该节点的值
                l += 1  # 将l移到下一个需要计算的位置
            if r & 1:
                r -= 1  # 移动r到下一个需要计算的位置
                sum += self.tree[r]  # 加上r左边的节点的值
            l >>= 1  # l移动到上一层
            r >>= 1  # r移动到上一层
        return sum

# 示例使用
arr = [1, 2, 3, 4, 5]
st = SegmentTree(arr)
print(st.query(1, 3))  # 查询索引1到2（不包括3）的区间和
st.update(2, 10)  # 将索引2的元素更新为10
print(st.query(1, 4))  # 查询索引1到3（不包括4）的区间和
```

<span id = "2"></span>

## Fenwick Tree

```Python
class FenwickTree:
    def __init__(self, size):
        self.size = size  # 树的大小，通常设为输入数组的长度
        self.tree = [0] * (size + 1)  # 初始化树，长度为size+1，因为索引0不使用

    def update(self, i, delta):
        """ 更新索引i处的值，增加delta """
        while i <= self.size:
            self.tree[i] += delta  # 将delta加到i对应的节点上
            i += i & -i  # 移动到下一个受影响的节点，即i加上i的二进制表示中最低位的1

    def query(self, i):
        """ 查询从开始到索引i的累积和 """
        sum = 0
        while i > 0:
            sum += self.tree[i]  # 累加从i到根的路径上的所有节点值
            i -= i & -i  # 移动到上一个节点，即i减去i的二进制表示中最低位的1
        return sum

# 示例使用
ft = FenwickTree(10)
ft.update(3, 7) # 在索引3处增加7
print(ft.query(5)) # 查询从开始到索引5的累积和，结果应该包括之前在索引3处增加的7
ft.update(1, 8) # 在索引1处增加8
print(ft.query(5)) # 再次查询从开始到索引5的累积和，现在应该包括索引1和3处的增加

```

## Transitive Closure - Floyd-WarShall Algorithm

```Python
def floyd_warshall(graph):
    """
    Floyd-Warshall algorithm to find shortest paths between all pairs of nodes.
    graph should be represented as a dictionary of dictionaries.
    """
    # 创建距离的二维数组
    dist = {u: {v: float('inf') for v in graph} for u in graph}

    # 初始化距离数组
    for u in graph:
        dist[u][u] = 0
        for v, w in graph[u].items():
            dist[u][v] = w

    # 逐对更新距离
    for k in graph:
        for u in graph:
            for v in graph:
                if dist[u][k] + dist[k][v] < dist[u][v]:
                    dist[u][v] = dist[u][k] + dist[k][v]

    return dist

# 示例使用
graph = {
    'A': {'B': 1, 'C': 4},
    'B': {'C': 1},
    'C': {}
}

# 计算所有顶点对之间的最短路径
distances = floyd_warshall(graph)
for source, targets in distances.items():
    for target, distance in targets.items():
        print(f"Shortest path from {source} to {target} is {distance}")

```

## Dijkstra's Algorithm

```Python
import heapq

def dijkstra(graph, start):
    # 初始化距离字典，所有的距离都设置为无穷大
    distances = {vertex: float('infinity') for vertex in graph}
    # 设置起始点的距离为0
    distances[start] = 0

    # 优先队列，用于选择下一个要访问的节点
    priority_queue = [(0, start)]

    while priority_queue:
        current_distance, current_vertex = heapq.heappop(priority_queue)

        # 如果当前节点的距离大于已知的最短距离，则跳过
        if current_distance > distances[current_vertex]:
            continue

        # 检查从当前顶点出发的所有邻接顶点
        for neighbor, weight in graph[current_vertex].items():
            distance = current_distance + weight

            # 如果找到更短的路径，则更新距离并将其加入优先队列
            if distance < distances[neighbor]:
                distances[neighbor] = distance
                heapq.heappush(priority_queue, (distance, neighbor))

    return distances

# 示例图
graph = {
    'A': {'B': 1, 'C': 4},
    'B': {'A': 1, 'C': 2, 'D': 5},
    'C': {'A': 4, 'B': 2, 'D': 1},
    'D': {'B': 5, 'C': 1}
}

# 计算所有顶点到'A'的最短路径
print(dijkstra(graph, 'A'))

```

## Prim-Jarnik's Algorithm

```Python
import heapq

def prim_algorithm(graph):
    """ Prim's algorithm to find the minimum spanning tree of a graph.
    :param graph: A dictionary representing the graph where keys are vertices
                  and values are lists of tuples (neighbor, weight).
    :return: Total weight of the minimum spanning tree and the tree itself as a list of edges.
    """
    # Initialize variables
    total_weight = 0
    min_span_tree = []
    visited = set()
    # Priority queue to select the edge with the minimum weight
    edges = [(0, None, 0)]  # (weight, from_vertex, to_vertex), start with an arbitrary vertex, e.g., vertex 0

    # While there are edges to process
    while edges:
        weight, frm, to = heapq.heappop(edges)

        # If the vertex hasn't been visited, add it to the tree
        if to not in visited:
            visited.add(to)
            total_weight += weight

            if frm is not None:
                min_span_tree.append((frm, to, weight))

            # Add all edges from this vertex to the priority queue
            for neighbor, edge_weight in graph[to]:
                if neighbor not in visited:
                    heapq.heappush(edges, (edge_weight, to, neighbor))

    return total_weight, min_span_tree

# Example usage
graph = {
    0: [(1, 2), (3, 6)],
    1: [(0, 2), (2, 3), (3, 8), (4, 5)],
    2: [(1, 3), (4, 7)],
    3: [(0, 6), (1, 8)],
    4: [(1, 5), (2, 7)]
}

total_weight, min_span_tree = prim_algorithm(graph)
total_weight, min_span_tree


```

## Kruskal's Algorithm

Kruskal 的算法是一种贪心算法，用于在加权无向图中找到最小生成树。它的核心思想是按照边的权重（从小到大）依次选择边，同时确保所选边不会形成环。

```Python
class DisjointSet:
    def __init__(self, vertices):
        self.parent = {v: v for v in vertices}
        self.rank = {v: 0 for v in vertices}

    def find(self, item):
        if self.parent[item] != item:
            self.parent[item] = self.find(self.parent[item])  # Path compression
        return self.parent[item]

    def union(self, set1, set2):
        root1 = self.find(set1)
        root2 = self.find(set2)

        if root1 != root2:
            if self.rank[root1] < self.rank[root2]:
                self.parent[root1] = root2
            elif self.rank[root1] > self.rank[root2]:
                self.parent[root2] = root1
            else:
                self.parent[root2] = root1
                self.rank[root1] += 1

def kruskal(graph_edges, vertices):
    # Initialize the disjoint set
    disjoint_set = DisjointSet(vertices)

    # Sort edges based on weight
    graph_edges.sort(key=lambda x: x[2])

    # Initialize minimum spanning tree
    min_span_tree = []
    total_weight = 0

    for edge in graph_edges:
        vertex1, vertex2, weight = edge

        # Check if adding this edge creates a cycle
        if disjoint_set.find(vertex1) != disjoint_set.find(vertex2):
            disjoint_set.union(vertex1, vertex2)
            min_span_tree.append(edge)
            total_weight += weight

    return total_weight, min_span_tree

# Example usage
graph_edges = [(0, 1, 10), (0, 2, 6), (0, 3, 5), (1, 3, 15), (2, 3, 4)]
vertices = [0, 1, 2, 3]

total_weight, min_span_tree = kruskal(graph_edges, vertices)
total_weight, min_span_tree


```

## Tarjan's Algorithm

```Python
def tarjan(graph):
    """ Tarjan's algorithm to find strongly connected components in a graph.
    :param graph: A dictionary representing the graph where keys are vertices and
                  values are lists of adjacent vertices.
    :return: List of strongly connected components.
    """
    index = 0
    stack = []
    on_stack = set()
    indices = {}
    low_links = {}
    result = []

    def strongconnect(v):
        nonlocal index
        indices[v] = index
        low_links[v] = index
        index += 1
        stack.append(v)
        on_stack.add(v)

        # Consider successors
        for w in graph[v]:
            if w not in indices:
                strongconnect(w)
                low_links[v] = min(low_links[v], low_links[w])
            elif w in on_stack:
                low_links[v] = min(low_links[v], indices[w])

        # If v is a root node, pop the stack and generate an SCC
        if low_links[v] == indices[v]:
            connected_component = []
            while True:
                w = stack.pop()
                on_stack.remove(w)
                connected_component.append(w)
                if w == v:
                    break
            result.append(connected_component)

    for v in graph:
        if v not in indices:
            strongconnect(v)

    return result

# Example usage
graph = {
    0: [1],
    1: [2],
    2: [0, 3],
    3: [4],
    4: [5],
    5: [3, 6],
    6: []
}

strongly_connected_components = tarjan(graph)
strongly_connected_components


```

## Bellman-Ford

Bellman-Ford 算法是一种用于在加权图中找到从单一源点到所有其他顶点的最短路径的算法。与 Dijkstra 算法相比，Bellman-Ford 算法的一个主要优势是它能够处理包含负权重边的图，并能检测图中是否存在负权重循环。

```Python
def bellman_ford(edges, vertex_count, source):
    # Initialize distance to all vertices as infinite and distance to source as 0
    distance = [float('inf')] * vertex_count
    distance[source] = 0

    # Relax all edges |V| - 1 times
    for _ in range(vertex_count - 1):
        for u, v, w in edges:
            if distance[u] != float('inf') and distance[u] + w < distance[v]:
                distance[v] = distance[u] + w

    # Check for negative-weight cycles
    for u, v, w in edges:
        if distance[u] != float('inf') and distance[u] + w < distance[v]:
            return "Graph contains negative weight cycle"

    return distance

# Example usage
edges = [(0, 1, -1), (0, 2, 4), (1, 2, 3), (1, 3, 2), (1, 4, 2), (3, 2, 5), (3, 1, 1), (4, 3, -3)]
vertex_count = 5  # Vertices are numbered from 0 to 4
source = 0

shortest_distances = bellman_ford(edges, vertex_count, source)
shortest_distances

```

Bellman-Ford 算法可以处理包含负权重边的图，并且能够正确计算出从单一源点到所有其他顶点的最短路径，即使这些路径包含负权重边。这是它相对于 Dijkstra 算法的一个显著优势。

但是，Bellman-Ford 算法的一个重要特性是它能够检测图中是否存在负权重循环。在图论中，负权重循环是一个问题，因为它意味着可以无限地减少从一点到另一点的“最短”路径的长度，从而使得最短路径问题没有实际的解答。

## A-star Algorithm

A*（读作“A 星”）算法是一种在图形平面上找到从起始点到目标点最短路径的有效算法。A*算法结合了最佳优先搜索和 Dijkstra 算法的特点，使用启发式方法提高搜索效率。它在许多领域，如计算机游戏、机器人路径规划和地理信息系统中，都有广泛应用。

算法原理
A\*算法通过维护一个优先级队列（通常实现为最小堆）来工作，队列中存储待处理的节点。每个节点的优先级由以下公式确定：

f(n) = g(n) + h(n)

其中：

f(n) 是节点 n 的总预估成本。
g(n) 是从起始点到节点 n 的实际成本。
h(n) 是从节点 n 到目标点的启发式估计成本（也称为启发式函数）。
启发式函数
启发式函数 h(n)对算法效率影响很大。为了保证算法能找到最短路径，h(n)必须是不过估计（即保守估计）的。常用的启发式函数包括欧几里得距离、曼哈顿距离等。

算法步骤
初始化：将起始节点放入优先级队列中。

循环处理：从队列中取出具有最低 f(n)值的节点。

如果这个节点是目标节点，则结束搜索。
否则，移动到所有相邻的节点，并更新它们的 g(n)和 f(n)值，然后将它们添加到队列中。
重复：重复步骤 2，直到找到目标节点或队列为空。

```Python
import heapq

def a_star(start, goal, neighbors, h):
    """
    A* algorithm implementation.

    :param start: The starting node.
    :param goal: The goal node.
    :param neighbors: Function to get the neighbors of a given node.
    :param h: Heuristic function that estimates the cost from a node to the goal.
    :return: The shortest path from start to goal.
    """
    # Priority queue
    open_set = []
    heapq.heappush(open_set, (0, start))

    # Cost from start to a node
    g_score = {start: 0}

    # For node n, came_from[n] is the node immediately preceding it on the cheapest path from start to n currently known.
    came_from = {}

    while open_set:
        current = heapq.heappop(open_set)[1]

        if current == goal:
            return reconstruct_path(came_from, current)

        for neighbor, cost in neighbors(current):
            tentative_g_score = g_score[current] + cost
            if neighbor not in g_score or tentative_g_score < g_score[neighbor]:
                came_from[neighbor] = current
                g_score[neighbor] = tentative_g_score
                f_score = tentative_g_score + h(neighbor, goal)
                heapq.heappush(open_set, (f_score, neighbor))

    return []  # Path not found

def reconstruct_path(came_from, current):
    """Reconstructs the path from start to the current node."""
    total_path = [current]
    while current in came_from:
        current = came_from[current]
        total_path.insert(0, current)
    return total_path

# Example heuristic function: Manhattan distance for a grid
def manhattan_distance(a, b):
    return abs(a[0] - b[0]) + abs(a[1] - b[1])

# Example usage
def example_neighbors(node):
    # For simplicity, assume a 2D grid where each node can move up, down, left, or right
    directions = [(0, 1), (0, -1), (-1, 0), (1, 0)]  # Up, down, left, right
    result = []
    for dx, dy in directions:
        neighbor = (node[0] + dx, node[1] + dy)
        result.append((neighbor, 1))  # Assume cost is always 1
    return result

start = (0, 0)
goal = (3, 3)
path = a_star(start, goal, example_neighbors, manhattan_distance)
path
```
